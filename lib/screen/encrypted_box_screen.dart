import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';

class EncryptedBoxScreen extends StatefulWidget {
  const EncryptedBoxScreen({Key? key}) : super(key: key);

  @override
  State<EncryptedBoxScreen> createState() => _EncryptedBoxScreenState();
}

class _EncryptedBoxScreenState extends State<EncryptedBoxScreen> {
  late final Box<String> encryptedBox;
  String? data = '';

  _getData() {
    setState(() {
      data = encryptedBox.get('secret');
    });
    log('Fetched data');
  }

  _putData() async {
    await encryptedBox.put('secret', 'Test secret key');
    log('Stored data');
  }

  @override
  void initState() {
    encryptedBox = Hive.box<String>('securedBox');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('$data'),
            SizedBox(height: 16),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ElevatedButton(
                  onPressed: _getData,
                  child: Text('Get'),
                ),
                ElevatedButton(
                  onPressed: _putData,
                  child: Text('Store'),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
