import 'package:demo_flutter_hive/model/person.dart';
import 'package:demo_flutter_hive/utils/xcore.dart';
import 'package:flutter/material.dart';

class UpdateScreen extends StatefulWidget {
  final int index;
  final Person person;
  const UpdateScreen({
    Key? key,
    required this.index,
    required this.person,
  }) : super(key: key);

  @override
  State<UpdateScreen> createState() => _UpdateScreenState();
}

class _UpdateScreenState extends State<UpdateScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text('Update Info'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: UpdatePersonForm(
          index: widget.index,
          person: widget.person,
        ),
      ),
    );
  }
}
